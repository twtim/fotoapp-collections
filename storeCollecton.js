Stores = new Mongo.Collection("stores");

StoresSchema = new SimpleSchema({
    name: {
        type: String,
        label: "Name",
        max: 20
    },
    address: {
        type: String,
        label: "Address",
        max: 200
    },
    postal : {
        type: String,
        label: "Postal",
        max: 200
    },
    place : {
        type: String,
        label: "Place",
        max: 200
    },
    email: {
        type: String,
        label: "Email",
        max: 200
    },
    createdAt: {
        type: Date,
        optional: true
    },
    updatedAt: {
        type: Date,
        optional: true
    },
    isDeleted: {
        type: Boolean,
        optional: true
    }
});

Stores.attachSchema(StoresSchema);

Stores.attachBehaviour("softRemovable");