Categories = new Mongo.Collection("categories");


CategoriesSchema = new SimpleSchema({
    name: {
        type: String,
        label: "Name",
        max: 20
    },
    description: {
        type: String,
        label: "Description",
        max: 200
    },
    imageId: {
        type: String,
        label: "Image",
        autoform: {
            afFieldInput: {
                type: "cfs-file",
                collection: "images"
            }
        }
    },
    createdAt: {
        type:Date
    },
    updatedAt: {
        type:Date
    },
    isDeleted: {
        type:Boolean
    }
});


Categories.attachSchema(CategoriesSchema);

Categories.attachBehaviour("softRemovable");