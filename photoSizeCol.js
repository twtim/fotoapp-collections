PhotoSizes = new Mongo.Collection("photoSize");

PhotoSizesSchema = new SimpleSchema({
    name: {
        type: String,
        label: "Name",
        max: 40
    },
    priceSlab:{
        type: [Object],
        label: "Price Slabs"
    },
    "priceSlab.$.price":{
        type: Number,
        label: "Price"
    },
    "priceSlab.$.from":{
        type: Number,
        label: "From"
    },
    "priceSlab.$.to":{
        type: Number,
        label: "To"
    },
    categoryId: {
        type:SimpleSchema.RegEx.Id,
        label: "Category",
        optional: true
    },
    createdAt: {
        type: Date,
        optional: true
    },
    updatedAt: {
        type: Date,
        optional: true
    },
    isDeleted: {
        type: Boolean,
        optional: true
    },
    isDefault: {
        type: Boolean
    }
});

PhotoSizes.attachSchema(PhotoSizesSchema);