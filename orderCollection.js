Orders = new Mongo.Collection("orders");

OrdersSchema = new SimpleSchema({
    title: {
        type: String,
        label: "Title",
        max: 20
    },
    name: {
        type: String,
        label: "Name",
        max: 200
    },
    email: {
        type: String,
        label: "Email",
        max: 200
    },
    tel: {
        type: String,
        label: "Tel",
        max: 200
    },
    street: {
        type: String,
        label: "Street",
        max: 200,
        optional: true
    },
    postCode: {
        type: String,
        label: "Post Code",
        max: 200,
        optional: true
    },
    residence: {
        type: String,
        label: "Residence",
        max: 200,
        optional: true
    },
    images: {
        type: [Object]
    },
    "images.$.imageId": {
        type: String
    },
    "images.$.count": {
        type: Number,
        min: 1
    },
    "images.$.size": {
        type: String,
        max: 200
    },
    "images.$.pricePerCopy": {
        type: Number
    },
    "category": {
        type: Object,
        label: "Category Object"
    },
    "category.name": {
        type: String,
        label: "Category Name"
    },
    "category.description": {
        type: String,
        label: "Category Description"
    },
    "paymentStatus": {
        type: String,
        max: 200
    },
    "paymentGateway": {
        type: String,
        max: 200,
        optional: true
    },
    "paymentData": {
        type: Object,
        blackbox: true,
        optional: true
    },
    settlement: {
        type: String,
        label: "Settlement"
    },
    location:{
        type: SimpleSchema.RegEx.Id,
        label: "Location",
        optional: true
    },
    completionDate: {
        type:Date,
        label: "Completion Date",
        optional: true
    },
    pickupPeriod: {
        type: String,
        label: "Pickup Time",
        optional: true
    },
    createdAt: {
        type: Date
    },
    updatedAt: {
        type: Date
    },
    status: {
        type: String
    }
});

Orders.attachSchema(OrdersSchema);
