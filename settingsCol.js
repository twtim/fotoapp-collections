Settings = new Mongo.Collection("settings");

SettingsSchema = new SimpleSchema({
    _id: {
        type: String,
        label: "Id",
        max: 200
    },
    notificationEmail: {
        type: String,
        label: "Email",
        max: 200
    }
});

Settings.attachSchema(SettingsSchema);